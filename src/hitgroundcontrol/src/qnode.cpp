/**
 * @file /src/qnode.cpp
 *
 * @brief Ros communication central!
 *
 * @date February 2011
 **/

/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <ros/network.h>
#include <string>
#include <stdio.h>
#include <std_msgs/String.h>
#include <nav_msgs/Odometry.h>
#include "../include/hitgroundcontrol/qnode.hpp"

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace hitgroundcontrol {

/*****************************************************************************
** Implementation
*****************************************************************************/
QNode::QNode(int argc, char** argv, uint16_t port):
        init_argc(argc),
        init_argv(argv),
        minPort(port)
{
  launchSteps = {"vins", "px4ctrl", "takeoff", "planner"};
  for (int i = 0; i < launchSteps.length(); i++) {
    QUdpSocket *socket = new QUdpSocket(this);
    socket->bind(minPort + i);
    udpMap[launchSteps[i]] = socket;
    isLaunched[launchSteps[i]] = false;
  }
  connect(udpMap[launchSteps[0]], SIGNAL(readyRead()), this, SLOT(onVinsReadyRead()));
  connect(udpMap[launchSteps[1]], SIGNAL(readyRead()), this, SLOT(onPx4ctrlReadyRead()));
  connect(udpMap[launchSteps[2]], SIGNAL(readyRead()), this, SLOT(onTakeoffReadyRead()));
  connect(udpMap[launchSteps[3]], SIGNAL(readyRead()), this, SLOT(onPlannerReadyRead()));

}
QNode::~QNode() {
  if(ros::isStarted()) {
    ros::shutdown(); // explicitly needed since we use ros::start();
    ros::waitForShutdown();
  }
  wait();
}

bool QNode::init() {
  ros::init(init_argc,init_argv,"hitgroundcontrol");
  if ( ! ros::master::check() ) {
    return false;
  }
  ros::start(); // explicitly needed since our nodehandle is going out of scope.
  ros::NodeHandle nh;
  // Add your ros communications here.
  //odom_sub = nh.subscribe<nav_msgs::Odometry>("/vins_estimator/imu_propagate", 10, &hitgroundcontrol::QNode::odom_subscribe_callback, this);
  odom_sub = nh.subscribe<nav_msgs::Odometry>("/vins_fusion/imu_propagate", 10, &hitgroundcontrol::QNode::odom_subscribe_callback, this);
  start();
  return true;
}

bool QNode::init(const std::string &master_url) {
  std::map<std::string,std::string> remappings;
  remappings["__master"] = master_url;
  ros::init(remappings,"hitgroundcontrol");
  if ( ! ros::master::check() ) {
    return false;
  }
  ros::start(); // explicitly needed since our nodehandle is going out of scope.
  ros::NodeHandle nh;
  // Add your ros communications here.
  odom_sub = nh.subscribe<nav_msgs::Odometry>("/vins_estimator/imu_propagate", 10, &hitgroundcontrol::QNode::odom_subscribe_callback, this);
  start();
  return true;
}

void QNode::run() {
  ros::Rate loop_rate(1);
  while ( ros::ok() ) {
    ros::spinOnce();
    loop_rate.sleep();
  }
  std::cout << "Ros shutdown, proceeding to close the gui." << std::endl;
  Q_EMIT rosShutdown(); // used to signal the gui for a shutdown (useful to roslaunch)
}
void QNode::launchCmd(const char *cmd) {
	// QString target_ip("127.0.0.1");
	QString target_ip("192.168.1.102");
  QHostAddress target_addr(target_ip);
  udpMap[cmd]->writeDatagram(cmd, target_addr, 9999);
  isLaunched[cmd] = true;
}
void QNode::terminateCmd(const char *cmd) {
	//QString target_ip("127.0.0.1");
	QString target_ip("192.168.1.102");
  QHostAddress target_addr(target_ip);
  char buf[20];
  strcpy(buf, cmd);
  strcat(buf, "_term");
  udpMap[cmd]->writeDatagram(buf, target_addr, 9999);
  isLaunched[cmd] = false;
}
void QNode::terminateAllCmd() {
  for (auto it = launchSteps.begin(); it != launchSteps.end(); it++) {
    terminateCmd(it->toStdString().c_str());
  }
}

void QNode::odom_subscribe_callback(const nav_msgs::Odometry::ConstPtr &msg) {
  // TODO: 处理频率很慢
  char buf[500];
  sprintf(buf, "pose.posiiton:\n\tx: %f\n\ty: %f\n\tz: %f\npose.orientation:\n\tx: %f\n\ty: %f\n\tz: %f\ntwist.linear:\n\tx: %f\n\ty: %f\n\tz: %f\ntwist.angular:\n\tx: %f\n\ty: %f\n\tz: %f\n",
    msg->pose.pose.position.x,
    msg->pose.pose.position.y,
    msg->pose.pose.position.z,
    msg->pose.pose.orientation.x,
    msg->pose.pose.orientation.y,
    msg->pose.pose.orientation.z,
    msg->twist.twist.linear.x,
    msg->twist.twist.linear.y,
    msg->twist.twist.linear.z,
    msg->twist.twist.angular.x,
    msg->twist.twist.angular.y,
    msg->twist.twist.angular.z
  );
  // char buf[100];
  // sprintf(buf, "%f %f %f", msg->pose.pose.position.x, msg->pose.pose.position.y, msg->pose.pose.position.z);
  Q_EMIT showOdom(buf);
}

void QNode::onVinsReadyRead() {
  char buf[512];
  qint64 len = udpMap["vins"]->pendingDatagramSize();
  udpMap["vins"]->readDatagram(buf, len);
  Q_EMIT showLog("vins", buf);
}
void QNode::onPx4ctrlReadyRead() {
  char buf[512];
  qint64 len = udpMap["px4ctrl"]->pendingDatagramSize();
  udpMap["px4ctrl"]->readDatagram(buf, len);
  Q_EMIT showLog("px4ctrl", buf);
}
void QNode::onTakeoffReadyRead() {
  char buf[512];
  qint64 len = udpMap["takeoff"]->pendingDatagramSize();
  udpMap["takeoff"]->readDatagram(buf, len);
  Q_EMIT showLog("takeoff", buf);
}
void QNode::onPlannerReadyRead() {
  char buf[512];
  qint64 len = udpMap["planner"]->pendingDatagramSize();
  udpMap["planner"]->readDatagram(buf, len);
  Q_EMIT showLog("planner", buf);
}
}  // namespace hitgroundcontrol
