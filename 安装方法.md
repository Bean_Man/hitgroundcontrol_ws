### 1.安装ros melodic
* sudo sh -c 'echo "deb https://mirrors.ustc.edu.cn/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
* sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
* sudo apt update
* sudo apt install ros-melodic-desktop-full
* sudo apt-get install python-rosinstall python-rosinstall-generator python-wstool build-essential
* sudo apt-get -y install python-rosdep
* echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
* source ~/.bashrc
* sudo rosdep init
* rosdep update

如果rosdep update失败，则参考博文https://blog.csdn.net/qq_35395195/article/details/122114402末尾提到的方法，注意将所有的h1c换成自己的用户名

### 2.安装qt5.9.9
* 在http://mirrors.ustc.edu.cn/qtproject/archive/qt/中科大镜像站下载5.9.9版本的qt-opensource-linux-x64-5.9.9.run
* 执行chmod +x qt-opensource-linux-x64-5.9.9.run
* 执行./qt-opensource-linux-x64-5.9.9.run
* 按照步骤安装即可,不知道选啥就全都勾选

### 3.安装功能包
* sudo apt install ros-melodic-qt-ros
* sudo apt install qtbase5-dev
* sudo apt install qtdeclarative5-dev

### 4.获取并运行代码
* git clone http://gitee.com/Bean_Man/hitgroundcontrol_ws
* cd hitgroundcontrol_ws
* catkin_make
* rosrun hitgroundcontrol hitgroundcontrol

### 5.开发工具安装(可选)
vscode + c/c++插件 + cmake插件 + ros插件
