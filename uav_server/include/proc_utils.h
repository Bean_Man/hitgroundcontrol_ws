#include <vector>
class ProcNode {
public:
    ProcNode(int pid);
    int get_ppid();
public:
    int m_pid;
    std::vector<ProcNode*> m_children;
};

class ProcTree {
public:
    ProcNode *root;
    ProcTree();
    std::vector<int> get_descendants_pids(int ppid);
    bool kill_descendants(int ppid);
};

void error_handling(char *message);